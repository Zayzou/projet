import time   #Librarie pour le livrable 2
import Adafruit_BBIO.GPIO as GPIO
import datetime

f=open("/home/debian/projet/datalivrable2.txt", "r") #Les data sont receuillis par le programme, elle viennent de l'application (formulaire)
if f.mode == 'r':
	contents =f.read()
	print("Contenu du fichier = ",contents) #Contenu total des data (separation des donnee selon notre protocole d'ecriture
	s =contents.split(":")

	heure_alarme=s[0]
	heure_alarme_total = int(heure_alarme)
	print("Heure = ",heure_alarme_total)

	minute_alarme=s[1]
	minute_alarme_total = int(minute_alarme)
	print("Minute = ",minute_alarme_total)

	flagbbb = s[2]
	flagweb = s[3]
	mode = s[4]
	print("Alarme BBB = ",flagbbb)
	print("Alarme WEB = ",flagweb)
	print("Mode = ",mode)
	GPIO.setup("GPIO0_20", GPIO.OUT) #Buzzer

	heure_rebours =s[0]
	minute_rebours =s[1]
	flag =1

	alarme_lundi = s[5]
	alarme_mardi = s[6]
	alarme_mercredi = s[7]
	alarme_jeudi = s[8]
	alarme_vendredi = s[9]
	alarme_samedi = s[10]
	alarme_dimanche = s[11]

	time.sleep(1)
	today = datetime.datetime.now() 
	if today.hour > 3 :
		heure_aujd = today.hour - 4  #Heure UTC vers EST
	elif today.hour == 0 :
		heure_aujd = 20
	elif today.hour == 1 :
		heure_aujd = 21
	elif today.hour == 2 :
		heure_aujd = 22
	elif today.hour == 3 :
		heure_aujd = 23
	minute_aujd = today.minute

	if (flag == 1): #Mise en memoire de l'heure actuel (une seul fois)
		mem_heure  = heure_aujd
		mem_minute  = minute_aujd
		flag = 0

	print("Il est",heure_aujd,"heure et",minute_aujd,"minute", "et on est le", today.weekday())
	print(" ")

	if(mode == '1'): #Dans le cas ou le mode = alarme. On verifie constament l'heure des data selon l'heure systeme et la date voulu afin de sonner le buzzer
		if ((heure_aujd == heure_alarme_total) and (minute_aujd == minute_alarme_total)) and (int(alarme_lundi)) and (today.weekday()==0):
			print("Lundi")
			GPIO.output("GPIO0_20", GPIO.HIGH)
			time.sleep(1)
			GPIO.output("GPIO0_20", GPIO.LOW)

		if ((heure_aujd == heure_alarme_total) and (minute_aujd == minute_alarme_total)) and (int(alarme_mardi)) and (today.weekday()==1):
			print("Mardi")
			GPIO.output("GPIO0_20", GPIO.HIGH)
			time.sleep(1)
			GPIO.output("GPIO0_20", GPIO.LOW)

		if ((heure_aujd == heure_alarme_total) and (minute_aujd == minute_alarme_total)) and (int(alarme_mercredi)) and (today.weekday()==2):
			print("Mercredi")
			GPIO.output("GPIO0_20", GPIO.HIGH)
			time.sleep(1)
			GPIO.output("GPIO0_20", GPIO.LOW)

		if ((heure_aujd == heure_alarme_total) and (minute_aujd == minute_alarme_total)) and (int(alarme_jeudi)) and (today.weekday()==3):
			print("Jeudi")
			GPIO.output("GPIO0_20", GPIO.HIGH)
			time.sleep(1)
			GPIO.output("GPIO0_20", GPIO.LOW)

		if ((heure_aujd == heure_alarme_total) and (minute_aujd == minute_alarme_total)) and (int(alarme_vendredi)) and (today.weekday()==4):
			print("Vendredi")
			GPIO.output("GPIO0_20", GPIO.HIGH)
			time.sleep(1)
			GPIO.output("GPIO0_20", GPIO.LOW)

		if ((heure_aujd == heure_alarme_total) and (minute_aujd == minute_alarme_total)) and (int(alarme_samedi)) and (today.weekday()==5):
			print("Samedi")
			GPIO.output("GPIO0_20", GPIO.HIGH)
			time.sleep(1)
			GPIO.output("GPIO0_20", GPIO.LOW)

		if ((heure_aujd == heure_alarme_total) and (minute_aujd == minute_alarme_total)) and (int(alarme_dimanche)) and (today.weekday()==6):
			print("Dimanche")
			GPIO.output("GPIO0_20", GPIO.HIGH)
			time.sleep(1)
			GPIO.output("GPIO0_20", GPIO.LOW)


	elif(mode =='2') : #Mode compte a rebours. Verification avec l'heure. ATTENTION LE MODE N'EST PAS FONCTIONNEL EN CRONTAB
		if ((heure_aujd == (int(heure_rebours) + mem_heure)) and (minute_aujd  == (int(minute_rebours) +mem_minute))):
			print("Count down")
			GPIO.output("GPIO0_20", GPIO.HIGH)
			time.sleep(1)
			GPIO.output("GPIO0_20", GPIO.LOW)



